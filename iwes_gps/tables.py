from enum import Enum

from piccolo.columns import Integer, Float, Varchar
from piccolo.table import Table
from piccolo.utils.pydantic import create_pydantic_model


class GPS_record(Table):
    class Status(str, Enum):
        valid = "T"
        invalid = "E"
        initializing = "I"

    token = Varchar(length=10)

    set1a = Integer()
    set1val = Float()
    set1status = Varchar(length=1, choices=Status)

    set2a = Integer()
    set2val = Float()
    set2status = Varchar(length=1, choices=Status)

    set3val = Float()
    set3status = Varchar(length=1, choices=Status)

    set4val = Float()
    set4status = Varchar(length=1, choices=Status)

    set5val = Float()
    set5status = Varchar(length=1, choices=Status)

    val1 = Float()
    val2 = Float()
    val3 = Float()
    val4 = Float()
    val5 = Float()
    val6 = Float()

    val7_raw = Varchar(length=20)

    @classmethod
    def validate(cls, **kwargs):
        model = create_pydantic_model(cls)
        return cls(**model(**kwargs).dict())
