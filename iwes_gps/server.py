import asyncio
import csv
from typing import Iterable, List, Tuple

from piccolo.columns import Column
from piccolo.engine import engine_finder
from pydantic import ValidationError

from .tables import GPS_record


class PrintProtocol(asyncio.DatagramProtocol):
    def datagram_received(self, data: bytes, addr: Tuple[str, int]) -> None:
        super().datagram_received(data, addr)
        for line in csv.reader(data.decode().splitlines()):
            print(line)


class DBInsertProtocol(asyncio.DatagramProtocol):
    def datagram_received(self, data: bytes, addr: Tuple[str, int]) -> None:
        super().datagram_received(data, addr)
        rows = [line for line in csv.reader(data.decode().splitlines())]
        self.insert(rows)

    @property
    def engine_t(self) -> str:
        if not hasattr(self, "_engine_t"):
            self._engine_t = engine_finder().engine_type
        return self._engine_t

    @property
    def cols(self) -> List[Column]:
        if not hasattr(self, "_cols"):
            _, *self._cols = GPS_record.all_columns()
        return self._cols

    def colname(self, col: Column) -> str:
        return col.get_select_string(self.engine_t).split(".")[-1]

    def record(self, row: List[str]):
        assert len(self.cols) == len(row)
        try:
            rec = GPS_record.validate(
                **{self.colname(col): val for col, val in zip(self.cols, row)}
            )
        except ValidationError as err:
            print(f"SKIP: {row}\n failed validation: {err}")
            return
        else:
            return rec

    def insert(self, rows: Iterable[List[str]]):
        records = GPS_record.insert()
        for row in rows:
            rec = self.record(row)
            if rec is None:
                continue
            records = records.add(rec)
        return records.run_sync()


async def main():
    await GPS_record.create_table(if_not_exists=True).run()

    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        DBInsertProtocol, ("0.0.0.0", 5000)
    )
    try:
        print("Server will listen on localhost:5000 for 20 mins")
        await asyncio.sleep(1200)
    finally:
        print("shutting down server")
        transport.close()
