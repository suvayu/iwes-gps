import asyncio
import os

from .server import main

os.environ["PICCOLO_CONF"] = "iwes_gps.piccolo_conf"

if __name__ == "__main__":
    asyncio.run(main())
