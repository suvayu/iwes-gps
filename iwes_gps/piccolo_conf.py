from piccolo.engine.postgres import PostgresEngine

DB = PostgresEngine(
    config={
        "host": "localhost",
        "database": "postgres",
        "user": "postgres",
        "password": "pgpasswd",
    }
)

# from pathlib import Path

# from piccolo.engine.sqlite import SQLiteEngine

# DBDIR = Path(__file__).parent / "db"
# DBDIR.mkdir(exist_ok=True)
# DB = SQLiteEngine(path=str(DBDIR / "mydb.sqlite"))
