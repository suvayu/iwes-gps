#!/bin/bash

# # debug
# set -o xtrace

declare opts="$1"
declare LOGDIR=${0%/*}/../logs
mkdir -p $LOGDIR
declare LOGFILE=$LOGDIR/serial.log PIDFILE=$LOGDIR/serial.pid

function status() {
    [[ -f $PIDFILE ]] && \
	{
	    echo PID file: $PIDFILE
	    echo
	    ps -fu -p $(cat $PIDFILE)
	    echo
	    PTY=$(head -1 $LOGFILE | cut -d' ' -f 7)
	    echo "to send data to localhost:5000 as UDP packets by writing to $PTY"
	}
}

function start() {
    socat -d -d pty,rawer UDP-SENDTO:localhost:5000 &> $LOGFILE &
    declare pid="$!"
    echo $pid > $PIDFILE
    # time for socat to finish
    sleep 1
}

function stop() {
    [[ -f $PIDFILE ]] && {
	kill -s SIGTERM $(cat $PIDFILE)
	rm -f $PIDFILE
    }
}

function help_msg() {
    echo -e "\e[1mUsage:\e[0m ${0##*/} [-h | help | status | start | stop | restart]"
}

case $opts in
    -h|help)
	help_msg
	;;
    status)
	status
	;;
    start)
	[[ -f $PIDFILE ]] && { \
	    echo Socket relay already running, \(pid: $(cat $PIDFILE)\)
	} || { start; status; } ;
	;;
    stop)
	stop
	;;
    restart)
	stop
	start
	;;
    *)
	echo Unknown option.
	help_msg
	exit 1
	;;
esac
