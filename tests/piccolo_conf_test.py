from pathlib import Path

from piccolo.conf.apps import AppRegistry
from piccolo.engine.sqlite import SQLiteEngine

DBDIR = Path(__file__).parent / "db"
DBDIR.mkdir(exist_ok=True)
DB = SQLiteEngine(path=str(DBDIR / "mydb.sqlite"))

APP_REGISTRY = AppRegistry(apps=[])
