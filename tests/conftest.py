import os

os.environ["PICCOLO_CONF"] = "tests.piccolo_conf_test"

import pytest

from piccolo.table import create_tables, drop_tables
from piccolo.conf.apps import table_finder

TABLES = table_finder(["iwes_gps.tables"])


@pytest.fixture
def sqlitedb():
    create_tables(*TABLES)
    yield
    drop_tables(*TABLES)
