from pathlib import Path

import pytest

from iwes_gps.server import DBInsertProtocol
from iwes_gps.tables import GPS_record


@pytest.mark.parametrize(
    "fname",
    [
        Path(__file__).parent.parent / "data.txt",
        Path(__file__).parent.parent / "bad-data.txt",
    ],
)
def test_protocol(fname, sqlitedb, capsys):
    data = fname.read_bytes()
    protocol = DBInsertProtocol()
    protocol.datagram_received(data, ("localhost", 5000))
    records = GPS_record.select().run_sync()
    bad_count = 1 if "bad" in fname.name else 0
    ok_count = len(data.decode().splitlines()) - bad_count
    assert len(records) == ok_count

    if "bad" in fname.name:
        captured = capsys.readouterr()
        assert "failed validation" in captured.out
