"""GPS ingester

"""

from pathlib import Path

from setuptools import setup, find_packages

requirements = Path("requirements.txt").read_text().strip().split("\n")

setup(
    name="iwes-gps",
    version="0.1.dev",
    description="A simple GPS data ingester",
    long_description=Path("README.md").read_text(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/suvayu/iwes",
    packages=find_packages(exclude=["doc", "tests", "tmp"]),
    install_requires=requirements,
    package_data={"iwes_gps": ["py.typed"]},
)
