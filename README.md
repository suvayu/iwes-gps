# GPS ingester

## Setup steps and how to run and test

1. Install the [official](https://hub.docker.com/_/postgres/) docker
   container from postgres.  You can use `docker` or `podman`.

   Instructions using `podman` (apart from the rootless setup,
   everything should be identical between `docker` and `podman`):

   ```
   $ podman create postgres # in the prompt for source, choose docker.io
   ```

   NOTE: If you are running rootless, please follow the instructions
   [here](https://github.com/containers/podman/blob/main/docs/tutorials/rootless_tutorial.md).
   After updating your `/etc/sub{uid,gid}` files, you will also need
   to run:

   ```
   $ podman system migrate
   ```

   To check if everything was configured correctly, run:

   ```
   $ podman unshare cat /proc/self/uid_map
   $ podman unshare cat /proc/self/gid_map
   ```

   Along with your usual uid, you should also see the uid range you
   added in `/etc/sub{uid,gid}`.

2. Start the container.  Providing a superuser password for the
   database is mandatory; I chose `pgpasswd`.

   ```
   $ podman run --env POSTGRES_PASSWORD=pgpasswd --publish 5432:5432 --detach <image-id>
   ```

   NOTE: the image-id has to come after the environment and port
   forwarding specification.  The image-id is printed in the terminal
   after the `podman create` command.  You can also find it later with
   `podman images`.

   The container entrypoint sets up the `postgres` user and database.

3. Create a socket relay with `socat` by creating a terminal device
   that forwards data written to it to `localhost:5000`.  Run the
   script `fakeserial.sh` to do this.  It should print out the name of
   the terminal device that was created, say it is `/dev/pts/10`.  Now
   you can send data to any server listening to `localhost:5000` by
   concatenating a data file to the terminal device.

   ```
   $ cat data.txt > /dev/pts/10
   ```

4. Now you can install the Python dependencies in a virtual
   environment with:

   ```
   $ pip install .
   ```

5. You can start the server to process the incoming data stream by
   running:

   ```
   $ python -m iwes_gps
   ```

6. Now send data to the server by concatenating the sample data file
   to the terminal device as shown in (3).

   ```
   $ cat data.txt > /dev/pts/10
   ```

7. The server implements a basic data validation scheme, you can test
   this by sending the bad data file `bad-data.txt`.  You will notice
   one of the rows isn't accepted and an error message is printed out
   in the terminal.

## Comments on `gpsd`

Initially I wanted to use `gpsd` as then I wouldn't need to parse the
NMEA string, and wouldn't need to decode the various fields. As `gpsd`
listens for a data stream on a connected serial device, or over the
network.  I tried to fake a serial device using a pseudo-terminal
device (use `pty` twice in the call to `socat` in `fakeserial.sh`),
but `gpsd` couldn't detect it.  I tried sending it over the network,
but didn't succeed.  I then tried the same with `gpsfake`, and tried
to connect to it using `gpscsv`, however I was getting blocked by
SELinux. I tried setting it to permissive, but still couldn't connect
to the server.  So eventually I decided to not spend any more time on
it.  And arrived at my current solution.

## Running tests

There are a couple of test functions under `tests/`.  These can be run
by calling `pytest` from the top level directory.  Note that it
creates a temporary SQLite db in the test directory (ideally this
should be in a emphemeral temporary directory).
